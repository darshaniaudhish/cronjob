const cron = require('node-cron');

const shell = require('shelljs');

cron.schedule("* * * * * *", function(){
    console.log("Scheduler running");
    if(shell.exec("node sayHi.js").code !==0 ){
        console.log("Something went wrong");
    }
})